import { Component, OnInit } from '@angular/core';
import { isNil } from "lodash";

import { QuizService } from '../services/quiz.service';
import { UtilsService } from '../services/utils.service';
import { SecurityService } from '../services/security.service';

import { Quiz } from '../services/model/quiz';

@Component({
  selector: 'quiz-list',
  templateUrl: './quiz-list.component.html',
  styleUrls: ['./quiz-list.component.scss']
})
export class QuizListComponent implements OnInit {

  quizArray: Quiz[];
  showGoToManager: boolean;

  constructor(
    private quizService: QuizService,
    private securityService: SecurityService) {

      this.showGoToManager = false;
  }

  ngOnInit() {

    this._initQuizList();
    this._checkGoToManager();
  }

  private _initQuizList() : void {

    this.quizService.listVisible().then((quizArray: Quiz[]) => {

      this.quizArray = quizArray;

    }).catch(UtilsService.handleError);
  }

  private _checkGoToManager() : void {

    this.securityService.checkAdmin().then((check: boolean) => {

      this.showGoToManager = check && (window.innerWidth > 768);

    }).catch(UtilsService.handleError);
  }

}
