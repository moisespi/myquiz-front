import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QuizManagerComponent } from './quiz-manager/quiz-manager.component';
import { QuizListComponent } from './quiz-list/quiz-list.component';
import { QuizMakerComponent } from './quiz-maker/quiz-maker.component';
import { LoginComponent } from './login/login.component';

import { AdminAuthGuardService } from './services/admin-auth-guard.service';
import { UserAuthGuardService } from './services/user-auth-guard.service';


const routes: Routes = [
  { path: 'quiz-manager', component: QuizManagerComponent, canActivate: [AdminAuthGuardService] },
  { path: 'quiz/:id', component:  QuizMakerComponent, canActivate: [UserAuthGuardService] },
  { path: 'login', component: LoginComponent },
  { path: '**', component: QuizListComponent, canActivate: [UserAuthGuardService] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
