import { Component, OnInit, Input } from '@angular/core';

import { FormGroup, AbstractControl } from "@angular/forms";

@Component({
  selector: 'my-inputcolor',
  templateUrl: './my-inputcolor.component.html',
  styleUrls: ['./my-inputcolor.component.scss']
})
export class MyInputcolorComponent implements OnInit {

  @Input() id: string;
  @Input() form: FormGroup;
  @Input() label: string;

  control: AbstractControl;

  constructor() { }

  ngOnInit() {
    
    this.control = this.form.controls[this.id];
  }

}
