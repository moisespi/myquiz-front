import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyInputcolorComponent } from './my-inputcolor.component';

describe('MyInputcolorComponent', () => {
  let component: MyInputcolorComponent;
  let fixture: ComponentFixture<MyInputcolorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyInputcolorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyInputcolorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
