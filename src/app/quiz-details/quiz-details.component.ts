import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { forEach, isNil } from "lodash";

import { Quiz } from '../services/model/quiz';
import { Question } from '../services/model/question';
import { Result } from '../services/model/result';

import { CommunicationService, CommunicatioType } from '../services/communication.service';
import { QuestionService } from '../services/question.service';
import { ResponseService } from '../services/response.service';
import { ResultService } from '../services/result.service';
import { UtilsService } from '../services/utils.service';
import { TranslateService } from '@ngx-translate/core';
import { ToasterService } from '../services/toaster.service';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'quiz-details',
  templateUrl: './quiz-details.component.html',
  styleUrls: ['./quiz-details.component.scss']
})
export class QuizDetailsComponent implements OnInit, OnDestroy {

  quiz: Quiz;

  questionArray: Question[];
  resultArray: Result[];

  quizSelectSubscription: Subscription;
  questionRemoveSubscription: Subscription;
  questionModifyClickSubscription: Subscription;

  addQuestionModalVisible: boolean;
  questionForm: FormGroup
  questionModalHeader: string;
  questionSaveBtnLabel: string;

  addResultModalVisible: boolean;
  resultForm: FormGroup
  resultModalHeader: string;
  resultSaveBtnLabel: string;

  constructor(
    private utilsService: UtilsService,
    private formBuilder: FormBuilder,
    private communicationService: CommunicationService,
    private translateService: TranslateService,
    private questionService: QuestionService,
    private responseService: ResponseService,
    private resultService: ResultService,
    private toasterService: ToasterService,
    private confirmationService: ConfirmationService) { }

  ngOnInit() {

    this.quizSelectSubscription = this.communicationService.observable(CommunicatioType.QUIZ_SELECTED).subscribe((quiz: Quiz) => {
      
      this.quiz = quiz;
      if(quiz == null) {
        this.clearQuestionArray();
      } else {
        this._initQuestionForm();
        this._initQuestionArray(quiz);
        this._initResultForm();
        this._initResultArray(quiz);
      }
    });

    this.questionRemoveSubscription = this._initQuestionArraySubscription();

    this.questionModifyClickSubscription = this._initQuestionModifyClickSubscription();
  }

  private _initQuestionArray(quiz: Quiz, questionId?: number) : void {
    
    this.questionService.list(quiz.id).then((questionArray: Question[]) => {

      if(isNil(questionId)) {
        this.questionArray = questionArray;
      } else {
        forEach(this.questionArray, (q1: Question, indx1: number) => {
          if(q1.id == questionId) {
            let found = false;
            forEach(questionArray, (q2: Question, indx2: number) => {
              if(q2.id == questionId) {
                this.questionArray[indx1] = questionArray[indx2];
                found = true;
              }
            });
            if(!found) {
              this.questionArray.splice(indx1, 1);
            }
          }
        });
      }
    });
  }

  private _initResultArray(quiz: Quiz) : void {
    
    this.resultService.list(quiz.id).then((resultArray: Result[]) => {

      this.resultArray = resultArray;
    });
  }

  private _initQuestionArraySubscription() : Subscription {

    return this.communicationService.observable(CommunicatioType.FILL_QUESTION_ARRAY).subscribe((questionId: number) => {
      this._initQuestionArray(this.quiz, questionId);
    })
  }

  private _initQuestionModifyClickSubscription() : Subscription {

    return this.communicationService.observable(CommunicatioType.QUESTION_MODIFY_CLICK).subscribe((question: Question) => {
      this.modifyQuestionClick(question);
    })
  }

  private _initResponseAddClickSubscription() : Subscription {

    return this.communicationService.observable(CommunicatioType.RESPONSE_ADD_CLICK).subscribe((question: Question) => {
      this.modifyQuestionClick(question);
    })
  }

  private clearQuestionArray() : void {
      this.questionArray = [];
  }

  private _initQuestionForm() : void {
    
    this.questionForm = this.formBuilder.group({
      id: [null, []],
      quizId: [null, []],
      question: [null, [Validators.required, Validators.maxLength(1024)]]
    });
  }

  private _initResultForm() : void {
    
    this.resultForm = this.formBuilder.group({
      id: [null, []],
      quizId: [null, []],
      result: [null, [Validators.required, Validators.maxLength(32)]],
      color: [null, [Validators.required, Validators.maxLength(16)]]
    });
  }

  ngOnDestroy() {

    this.quizSelectSubscription.unsubscribe();
    this.questionRemoveSubscription.unsubscribe();
    this.questionModifyClickSubscription.unsubscribe();
  }

  addQuestionClick() : void {

    this.questionForm.reset();
    this.questionForm.patchValue({ quizId: this.quiz.id });
    this.questionModalHeader = this.translateService.instant("quiz-details.addQuestionDialog.headerAdd");
    this.questionSaveBtnLabel = this.translateService.instant("basic.add");

    this.addQuestionModalVisible = true;
  }

  addResultClick() : void {

    this.resultForm.reset();
    this.resultForm.patchValue({ quizId: this.quiz.id });
    this.resultModalHeader = this.translateService.instant("quiz-details.addResultDialog.headerAdd");
    this.resultSaveBtnLabel = this.translateService.instant("basic.add");

    this.addResultModalVisible = true;
  }

  modifyQuestionClick(question: Question) : void {

    this.questionForm.reset();
    this.questionForm.patchValue(question);
    this.questionModalHeader = this.translateService.instant("quiz-details.addQuestionDialog.headerModify");
    this.questionSaveBtnLabel = this.translateService.instant("basic.modify");

    this.addQuestionModalVisible = true;
  }

  modifyResultClick(question: Question) : void {

    this.resultForm.reset();
    this.resultForm.patchValue(question);
    this.resultModalHeader = this.translateService.instant("quiz-details.addQuestionDialog.headerModify");
    this.resultSaveBtnLabel = this.translateService.instant("basic.modify");

    this.addResultModalVisible = true;
  }
  
  addQuestionModalCreate() : void {

    this.utilsService.validateForm(this.questionForm).then((question: Question) => {
      
      if(isNil(question.id)) {

        this.questionService.create(question).then((question: Question) => {
  
          this._initQuestionArray(this.quiz);
          this.communicationService.notify(CommunicatioType.FILL_QUIZ_MENU);
          this.addQuestionModalVisible = false;
          this.toasterService.success(this.translateService.instant("toast.questionAdded"));
    
        }).catch(e => this.toasterService.error(e.error));
      } else {
          
        this.questionService.update(question).then((question: Question) => {
    
          this._initQuestionArray(this.quiz);
          this.addQuestionModalVisible = false;
          this.toasterService.success(this.translateService.instant("toast.questionUpdated"));
    
        }).catch(e => this.toasterService.error(e.error));
      }
    });
  }
  
  addResultModalCreate() : void {

    this.utilsService.validateForm(this.resultForm).then((result: Result) => {
      
      if(isNil(result.id)) {

        this.resultService.create(result).then((result: Result) => {
  
          this._initQuestionArray(this.quiz);
          this._initResultArray(this.quiz);
          this.communicationService.notify(CommunicatioType.FILL_QUIZ_MENU);
          this.addResultModalVisible = false;
          this.toasterService.success(this.translateService.instant("toast.resultAdded"));
    
        }).catch(e => this.toasterService.error(e.error));
      } else {
          
        this.resultService.update(result).then((result: Result) => {
    
          this._initQuestionArray(this.quiz);
          this._initResultArray(this.quiz);
          this.addResultModalVisible = false;
          this.toasterService.success(this.translateService.instant("toast.resultUpdated"));
    
        }).catch(e => this.toasterService.error(e.error));
      }
    });
  }

  removeResultClick(result: Result) : void {

    this.confirmationService.confirm({
        message: this.translateService.instant('confirmation.deleteResultMessage'),
        accept: () => {
          
          this.resultService.delete(result).then((result: Result) => {
      
            this._initResultArray(this.quiz);
            this.communicationService.notify(CommunicatioType.FILL_QUIZ_MENU);
            this.toasterService.success(this.translateService.instant("toast.resultRemoved"));
      
          }).catch(e => this.toasterService.error(e.error));
        }
    });
  }

  addQuestionModalCancel() : void {

    this.addQuestionModalVisible = false;
  }

  addResultModalCancel() : void {

    this.addResultModalVisible = false;
  }


}
