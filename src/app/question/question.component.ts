import { Component, OnInit, Input } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { isNil } from "lodash";

import { QuestionService } from '../services/question.service';
import { ResponseService } from '../services/response.service';
import { ResultService } from '../services/result.service';
import { ResponseResultService } from '../services/response-result.service';
import { ToasterService } from '../services/toaster.service';
import { CommunicationService, CommunicatioType } from '../services/communication.service';
import { UtilsService } from '../services/utils.service';

import { Question } from '../services/model/question';
import { Response } from '../services/model/response';
import { ResponseResult } from '../services/model/response-result';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Result } from '../services/model/result';

@Component({
  selector: 'question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {

  @Input() question: Question;

  responseArray: Response[];

  addResponseModalVisible: boolean;
  addResultModalVisible: boolean;
  responseForm: FormGroup
  responseModalHeader: string;
  responseSaveBtnLabel: string;

  responseResultForm: FormGroup;
  resultOptions: any[];
  responseResultArray: ResponseResult;

  selectedResponse: Response;

  constructor(
    private questionService: QuestionService,
    private responseService: ResponseService,
    private resultService: ResultService,
    private responseResultService: ResponseResultService,
    private toasterService: ToasterService,
    private communicationService: CommunicationService,
    private confirmationService: ConfirmationService,
    private translateService: TranslateService,
    private formBuilder: FormBuilder,
    private utilsService: UtilsService) { }

  ngOnInit() {
    
    this._initResponseForm();

    this._initResponseResultForm();

    this._initResponseArray();
  }

  private _initResponseForm() : void {
    
    this.responseForm = this.formBuilder.group({
      id: [null, []],
      questionId: [, []],
      response: [null, [Validators.required, Validators.maxLength(1024)]]
    });
  }

  private _initResponseResultForm() : void {
    
    this.responseResultForm = this.formBuilder.group({
      resultId: [null, [Validators.required]],
      responseId: [null, [Validators.required]],
      weight: [5, [Validators.required]]
    });
  }

  private _initResponseArray() : void {

    this.responseService.list(this.question.id).then((responseArray: Response[]) => {
      this.responseArray = responseArray;
    })
  }
  
  modifyQuestionClick(question: Question) : void {

    this.communicationService.notify(CommunicatioType.QUESTION_MODIFY_CLICK, question);
  }

  removeQuestionClick(question: Question) : void {

    this.confirmationService.confirm({
        message: this.translateService.instant('confirmation.deleteQuestionMessage'),
        accept: () => {
          
          this.questionService.delete(question).then((question: Question) => {
      
            this.communicationService.notify(CommunicatioType.FILL_QUESTION_ARRAY, this.question.id);
            this.communicationService.notify(CommunicatioType.FILL_QUIZ_MENU);
            this.toasterService.success(this.translateService.instant("toast.questionRemoved"));
      
          }).catch(e => this.toasterService.error(e.error));
        }
    });
  }

  addResponseClick(question: Question) : void {

    this.responseForm.reset();
    this.responseForm.patchValue({ questionId: question.id });
    this.responseModalHeader = this.translateService.instant("quiz-details.addResponseDialog.headerAdd");
    this.responseSaveBtnLabel = this.translateService.instant("basic.add");

    this.addResponseModalVisible = true;
  }
  
  addResponseModalCreate() : void {

    this.utilsService.validateForm(this.responseForm).then((response: Response) => {
      
      if(isNil(response.id)) {

        this.responseService.create(response).then((response: Response) => {
  
          this._initResponseArray();
          this.communicationService.notify(CommunicatioType.FILL_QUIZ_MENU);
          this.communicationService.notify(CommunicatioType.FILL_QUESTION_ARRAY, this.question.id);
          this.addResponseModalVisible = false;
          this.toasterService.success(this.translateService.instant("toast.responseAdded"));
    
        }).catch(e => this.toasterService.error(e.error));
      } else {
          
        this.responseService.update(response).then((response: Response) => {
    
          this._initResponseArray();
          this.addResponseModalVisible = false;
          this.toasterService.success(this.translateService.instant("toast.responseUpdated"));
    
        }).catch(e => this.toasterService.error(e.error));
      }
    });
  }

  modifyResponseClick(response: Response) : void {

    this.responseForm.reset();
    this.responseForm.patchValue(response);
    this.responseModalHeader = this.translateService.instant("quiz-details.addResponseDialog.headerModify");
    this.responseSaveBtnLabel = this.translateService.instant("basic.modify");

    this.addResponseModalVisible = true;
  }

  removeResponseClick(response: Response) : void {

    this.confirmationService.confirm({
        message: this.translateService.instant('confirmation.deleteResponseMessage'),
        accept: () => {
          
          this.responseService.delete(response).then((response: Response) => {
      
            this._initResponseArray();
            this.communicationService.notify(CommunicatioType.FILL_QUIZ_MENU);
            this.communicationService.notify(CommunicatioType.FILL_QUESTION_ARRAY, this.question.id);
            this.toasterService.success(this.translateService.instant("toast.responseRemoved"));
      
          }).catch(e => this.toasterService.error(e.error));
        }
    });
  }

  addResponseModalCancel() : void {

    this.addResponseModalVisible = false;
  }

  private _initResponseResultArray() : void {

    this.resultService.map({
      quizId: this.question.quizId, responseId: this.selectedResponse.id
    }).then((dto: any) => {

      this.resultOptions = UtilsService.toDropdownOptions(dto.missing, 'result', 'id');
      this.responseResultArray = dto.existing;

      this.responseResultForm.controls.resultId.patchValue(isNil(this.resultOptions[0]) ? null : this.resultOptions[0].value);

    }).catch(e => this.toasterService.error(e.error));
  }

  resultModalOpen(
    response: Response) : void {

      this.selectedResponse = response;
      this.responseResultForm.patchValue({ responseId: response.id });

      this._initResponseResultArray();

      this.addResultModalVisible = true;
  }

  resultModalClose() : void {

    this.addResultModalVisible = false;
  }

  addResponseResult() : void {

    this.utilsService.validateForm(this.responseResultForm).then((responseResult: ResponseResult) => {

        this.responseResultService.create(responseResult).then((responseResult: ResponseResult) => {
  
          this._initResponseArray();
          this._initResponseResultArray();
          this.communicationService.notify(CommunicatioType.FILL_QUIZ_MENU);
          this.toasterService.success(this.translateService.instant("toast.responseResultAdded"));
    
        }).catch(e => this.toasterService.error(e.error));

    });
  }

  removeResponseResultClick(
    responseResult: ResponseResult) : void {

    this.confirmationService.confirm({
        message: this.translateService.instant('confirmation.deleteResponseResultMessage'),
        accept: () => {
          
          this.responseResultService.delete(responseResult).then((responseResult: ResponseResult) => {
      
            this._initResponseArray();
            this._initResponseResultArray();
            this.communicationService.notify(CommunicatioType.FILL_QUIZ_MENU);
            this.toasterService.success(this.translateService.instant("toast.responseResultRemoved"));
      
          }).catch(e => this.toasterService.error(e.error));
        }
    });
  }

  responseAsTitle(response: string) : string {

    let UMBRAL: number = 50;

    let length: number = response.length;
    if(length > UMBRAL) {
      return response.substr(0, UMBRAL - 2) + '..';
    } else {
      return response;
    }
  }

}
