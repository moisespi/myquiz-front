import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
import defaultLanguage from "../assets/i18n/es.json";

import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  
  constructor(
    private translate: TranslateService,
    private titleService: Title) {

      translate.setTranslation('es', defaultLanguage);
      translate.setDefaultLang('es');

      this.titleService.setTitle("Cinco Movimientos");
  }

  ngOnInit() : void {
  }
  
}
