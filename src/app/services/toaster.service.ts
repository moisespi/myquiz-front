import { Injectable } from '@angular/core';

import { MessageService } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class ToasterService {

  constructor(
    private messageService: MessageService) { }

  public success(message: string) : void {
    this.messageService.add({ key: 'main', severity:'success', summary: message, sticky: false });
  }

  public info(message: string) : void {
    this.messageService.add({ key: 'main', severity:'info', summary: message, sticky: false });
  }

  public warning(message: string) : void {
    this.messageService.add({ key: 'main', severity:'warn', summary: message, sticky: false });
  }

  public error(message: string) : void {
    this.messageService.add({ key: 'main', severity:'error', summary: message, sticky: true });
  }
}
