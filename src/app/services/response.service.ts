import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { UtilsService } from 'src/app/services/utils.service';
import { PropertiesEnum } from 'src/app/services/model/properties.enum';
import { Response } from 'src/app/services/model/response';

@Injectable({
  providedIn: 'root'
})
export class ResponseService {

  constructor(
    private http: HttpClient) { }

    private readonly RESPONSE_API = PropertiesEnum.CONTEXT_PATH + '/response';
  
    list(
      questionId: number): Promise<Response[]> {
  
      return this.http
        .get(this.RESPONSE_API + '/list/' + questionId, UtilsService.getHeaders())
        .toPromise()
        .then(UtilsService.handleResponse)
        .catch(UtilsService.handleError);
    }
  
    create(response: Response): Promise<Response> {
  
      return this.http
        .post(this.RESPONSE_API + '/create', response, UtilsService.getHeaders())
        .toPromise()
        .then(UtilsService.handleResponse)
        .catch(UtilsService.handleError);
    }
  
    update(response: Response): Promise<Response> {
  
      return this.http
        .post(this.RESPONSE_API + '/update', response, UtilsService.getHeaders())
        .toPromise()
        .then(UtilsService.handleResponse)
        .catch(UtilsService.handleError);
    }
  
    delete(response: Response): Promise<Response> {
  
      return this.http
        .post(this.RESPONSE_API + '/delete', response, UtilsService.getHeaders())
        .toPromise()
        .then(UtilsService.handleResponse)
        .catch(UtilsService.handleError);
    }
}
