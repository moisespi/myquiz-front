import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { UtilsService } from 'src/app/services/utils.service';
import { PropertiesEnum } from 'src/app/services/model/properties.enum';
import { ResponseResult } from 'src/app/services/model/response-result';

@Injectable({
  providedIn: 'root'
})
export class ResponseResultService {

  constructor(
    private http: HttpClient) { }

    private readonly RESPONSE_RESULT_API = PropertiesEnum.CONTEXT_PATH + '/responseResult';
  
    listByResponse(
      responseId: number): Promise<Response[]> {
  
      return this.http
        .get(this.RESPONSE_RESULT_API + '/listByResponse/' + responseId, UtilsService.getHeaders())
        .toPromise()
        .then(UtilsService.handleResponse)
        .catch(UtilsService.handleError);
    }
  
    listByResult(
      resultId: number): Promise<Response[]> {
  
      return this.http
        .get(this.RESPONSE_RESULT_API + '/listByResult/' + resultId, UtilsService.getHeaders())
        .toPromise()
        .then(UtilsService.handleResponse)
        .catch(UtilsService.handleError);
    }
  
    create(responseResult: ResponseResult): Promise<ResponseResult> {
  
      return this.http
        .post(this.RESPONSE_RESULT_API + '/create', responseResult, UtilsService.getHeaders())
        .toPromise()
        .then(UtilsService.handleResponse)
        .catch(UtilsService.handleError);
    }
  
    update(responseResult: ResponseResult): Promise<ResponseResult> {
  
      return this.http
        .post(this.RESPONSE_RESULT_API + '/update', responseResult, UtilsService.getHeaders())
        .toPromise()
        .then(UtilsService.handleResponse)
        .catch(UtilsService.handleError);
    }
  
    delete(responseResult: ResponseResult): Promise<ResponseResult> {
  
      return this.http
        .post(this.RESPONSE_RESULT_API + '/delete', responseResult, UtilsService.getHeaders())
        .toPromise()
        .then(UtilsService.handleResponse)
        .catch(UtilsService.handleError);
    }
}
