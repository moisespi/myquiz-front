import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { UtilsService } from 'src/app/services/utils.service';
import { PropertiesEnum } from 'src/app/services/model/properties.enum';
import { Quiz } from 'src/app/services/model/quiz';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  constructor(
    private http: HttpClient) { }

  private readonly QUIZ_API = PropertiesEnum.CONTEXT_PATH + '/quiz';

  list(): Promise<Quiz[]> {

    return this.http
      .get(this.QUIZ_API + '/list', UtilsService.getHeaders())
      .toPromise()
      .then(UtilsService.handleResponse)
      .catch(UtilsService.handleError);
  }

  listVisible(): Promise<Quiz[]> {

    return this.http
      .get(this.QUIZ_API + '/listVisible', UtilsService.getHeaders())
      .toPromise()
      .then(UtilsService.handleResponse)
      .catch(UtilsService.handleError);
  }

  fullGet(
    quizId: number): Promise<Quiz> {

    return this.http
      .get(this.QUIZ_API + '/fullGet/' + quizId, UtilsService.getHeaders())
      .toPromise()
      .then(UtilsService.handleResponse)
      .catch(UtilsService.handleError);
  }

  download(
    quizId: number, param: any): Promise<any> {

    return this.http
      .post(this.QUIZ_API + '/download/' + quizId, param, UtilsService.getHeaders())
      .toPromise()
      .then(UtilsService.handleResponse)
      .catch(UtilsService.handleError);
  }

  create(quiz: Quiz): Promise<Quiz> {

    return this.http
      .post(this.QUIZ_API + '/create', quiz, UtilsService.getHeaders())
      .toPromise()
      .then(UtilsService.handleResponse)
      .catch(UtilsService.handleError);
  }

  update(quiz: Quiz): Promise<Quiz> {

    return this.http
      .post(this.QUIZ_API + '/update', quiz, UtilsService.getHeaders())
      .toPromise()
      .then(UtilsService.handleResponse)
      .catch(UtilsService.handleError);
  }

  updateVisible(quiz: Quiz): Promise<Quiz> {

    return this.http
      .post(this.QUIZ_API + '/updateVisible', quiz, UtilsService.getHeaders())
      .toPromise()
      .then(UtilsService.handleResponse)
      .catch(UtilsService.handleError);
  }

  delete(quiz: Quiz): Promise<Quiz> {

    return this.http
      .post(this.QUIZ_API + '/delete', quiz, UtilsService.getHeaders())
      .toPromise()
      .then(UtilsService.handleResponse)
      .catch(UtilsService.handleError);
  }
}
