import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { UtilsService } from 'src/app/services/utils.service';
import { PropertiesEnum } from 'src/app/services/model/properties.enum';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  constructor(
    private http: HttpClient) { }

  private readonly SECURITY_API = PropertiesEnum.CONTEXT_PATH + '/security';

  checkAdmin(): Promise<boolean> {

    return this.http
      .get(this.SECURITY_API + '/checkAdmin', UtilsService.getHeaders())
      .toPromise()
      .then(UtilsService.handleResponse)
      .catch(UtilsService.handleError);
  }

  checkUser(): Promise<boolean> {

    return this.http
      .get(this.SECURITY_API + '/checkUser', UtilsService.getHeaders())
      .toPromise()
      .then(UtilsService.handleResponse)
      .catch(UtilsService.handleError);
  }

  checkPassword(
    password: string): Promise<boolean> {

    return this.http
      .post(this.SECURITY_API + '/checkPassword', password)
      .toPromise()
      .then(UtilsService.handleResponse)
      .catch(UtilsService.handleError);
  }
}
