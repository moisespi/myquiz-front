import { TestBed } from '@angular/core/testing';

import { ResponseResultService } from './response-result.service';

describe('ResponseResultService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ResponseResultService = TestBed.get(ResponseResultService);
    expect(service).toBeTruthy();
  });
});
