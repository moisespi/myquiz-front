import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';

import { SecurityService } from 'src/app/services/security.service';
import { ToasterService } from '../services/toaster.service';

@Injectable({
  providedIn: 'root'
})
export class UserAuthGuardService implements CanActivate {

  constructor(
    private router: Router,
    private securityService: SecurityService,
    private toasterService: ToasterService) { }

  canActivate(): Observable<boolean> {
    
    return new Observable<boolean>(obs => {

      this.securityService.checkUser().then((check: boolean) => {
  
        if(check) {
          obs.next(true);
        } else {
          this.router.navigateByUrl('/login');
          obs.next(false);
        }
  
      }).catch(e => this.toasterService.error(e.error));
    });
  }
}
