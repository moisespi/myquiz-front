import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { UtilsService } from 'src/app/services/utils.service';
import { PropertiesEnum } from 'src/app/services/model/properties.enum';
import { Result } from 'src/app/services/model/result';

@Injectable({
  providedIn: 'root'
})
export class ResultService {

  constructor(
    private http: HttpClient) { }

  private readonly RESULT_API = PropertiesEnum.CONTEXT_PATH + '/result';

  list(
    resultId: number): Promise<Result[]> {

    return this.http
      .get(this.RESULT_API + '/list/' + resultId, UtilsService.getHeaders())
      .toPromise()
      .then(UtilsService.handleResponse)
      .catch(UtilsService.handleError);
  }

  map(
    dto: any): Promise<any> {

    return this.http
      .post(this.RESULT_API + '/map', dto, UtilsService.getHeaders())
      .toPromise()
      .then(UtilsService.handleResponse)
      .catch(UtilsService.handleError);
  }

  create(result: Result): Promise<Result> {

    return this.http
      .post(this.RESULT_API + '/create', result, UtilsService.getHeaders())
      .toPromise()
      .then(UtilsService.handleResponse)
      .catch(UtilsService.handleError);
  }

  update(result: Result): Promise<Result> {

    return this.http
      .post(this.RESULT_API + '/update', result, UtilsService.getHeaders())
      .toPromise()
      .then(UtilsService.handleResponse)
      .catch(UtilsService.handleError);
  }

  delete(result: Result): Promise<Result> {

    return this.http
      .post(this.RESULT_API + '/delete', result, UtilsService.getHeaders())
      .toPromise()
      .then(UtilsService.handleResponse)
      .catch(UtilsService.handleError);
  }
}
