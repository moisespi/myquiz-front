
import { ResponseResult } from 'src/app/services/model/response-result';

export class Response {

    id: number;
    response: string;
    questionId: number;
    responseResults: ResponseResult[];
}