
import { Response } from 'src/app/services/model/response';

export class Question {

    id: number;
    question: string;
    quizId: number;
    responses:  Response[];
    valid?: boolean;
}