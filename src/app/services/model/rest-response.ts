
export class RestResponse<T> {

    success: boolean;
    response: T;
    extra: any;
    error: string;
}