
import { Question } from 'src/app/services/model/question';

export class Quiz {

    id: number;
    name: string;
    valid: boolean;
    visible: boolean;
    questions: Question[];
}