
export class Result {

    id: number;
    result: string;
    color: string;
    quizId: number;
}