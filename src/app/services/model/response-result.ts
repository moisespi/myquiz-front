
export class ResponseResult {

    id: number;
    responseId: number;
    resultId: number;
    result: string;
    weight: number;
    color: string;
}