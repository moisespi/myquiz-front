import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter } from 'rxjs/operators'; 

@Injectable({
  providedIn: 'root'
})
export class CommunicationService {

  private messageSource = new BehaviorSubject('default message');
  currentMessage = this.messageSource.asObservable();

  private notifiers: any;

  constructor() {
    this.notifiers = {};
  }

  notify(type: CommunicatioType, value?: any) : void {
    this._initNotification(type);
    this.notifiers[type].notifier.next(value === undefined ? true : value);
  }

  observable(type: CommunicatioType) : Observable<any> {
    this._initNotification(type);
    return this.notifiers[type].observable.pipe(filter((value) => { return value !== undefined }));
  }

  private _initNotification(type: CommunicatioType) {
    if(this.notifiers[type] == undefined) {
      let notifier: BehaviorSubject<any> = new BehaviorSubject(undefined);
      this.notifiers[type] = {
        notifier: notifier,
        observable: notifier.asObservable()
      };
    }
  }
  
}

export enum CommunicatioType {
  QUIZ_SELECTED = 0,
  QUESTION_MODIFY_CLICK = 1,
  FILL_QUESTION_ARRAY = 2,
  RESPONSE_ADD_CLICK = 3,
  FILL_QUIZ_MENU = 4
}
