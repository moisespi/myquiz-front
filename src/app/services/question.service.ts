import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { UtilsService } from 'src/app/services/utils.service';
import { PropertiesEnum } from 'src/app/services/model/properties.enum';
import { Question } from 'src/app/services/model/question';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  constructor(
    private http: HttpClient) { }

  private readonly QUIZ_API = PropertiesEnum.CONTEXT_PATH + '/question';

  list(
    quizId: number): Promise<Question[]> {

    return this.http
      .get(this.QUIZ_API + '/list/' + quizId, UtilsService.getHeaders())
      .toPromise()
      .then(UtilsService.handleResponse)
      .catch(UtilsService.handleError);
  }

  create(question: Question): Promise<Question> {

    return this.http
      .post(this.QUIZ_API + '/create', question, UtilsService.getHeaders())
      .toPromise()
      .then(UtilsService.handleResponse)
      .catch(UtilsService.handleError);
  }

  update(question: Question): Promise<Question> {

    return this.http
      .post(this.QUIZ_API + '/update', question, UtilsService.getHeaders())
      .toPromise()
      .then(UtilsService.handleResponse)
      .catch(UtilsService.handleError);
  }

  delete(question: Question): Promise<Question> {

    return this.http
      .post(this.QUIZ_API + '/delete', question, UtilsService.getHeaders())
      .toPromise()
      .then(UtilsService.handleResponse)
      .catch(UtilsService.handleError);
  }

}