import { Injectable } from '@angular/core';
import { FormGroup, AbstractControl } from "@angular/forms";
import { forOwn } from "lodash";
import { HttpHeaders } from '@angular/common/http';

import { RestResponse } from 'src/app/services/model/rest-response';

import { PropertiesEnum } from 'src/app/services/model/properties.enum';

import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor(
    private translateService: TranslateService) { }
  
  static handleResponse(response: RestResponse<any>): Promise<any> {
    
    if(response.success) {
      return Promise.resolve(response.response);
    }
    return Promise.reject(response.error);
  }
  
  static handleError(response: any): Promise<any> {

    let errorMessage: string = response.error || response || response.json && response.json();
    
    return Promise.reject(errorMessage);
  }

  static markAllAsDirty(form : FormGroup) : void {

    forOwn(form.controls, (control: AbstractControl, key: string) => {

      control.markAsDirty();
    });
  }

  static toDropdownOptions(
    list : any[],
    label: string, value: string) : any[] {

    return list.map(e => { return { label: e[label], value: e[value] } });
  }

  validateForm(form : FormGroup) : Promise<any> {

    if(form.valid) {

      let value: any = form.getRawValue();

      return Promise.resolve(value);
    }

    UtilsService.markAllAsDirty(form);

    return Promise.reject(this.translateService.instant('utils-service.valdiationErrors'));
  }

  static getHeaders(): { headers?: HttpHeaders } {

    let headers = new HttpHeaders().
      set('Content-Type', 'application/json').
      set('Authorization', localStorage[PropertiesEnum.LOCAL_STORAGE_PASSWORD] || '');

    return { headers: headers };
  }
}
