import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { forEach, isNil } from "lodash";

import { QuizService } from '../services/quiz.service';
import { ToasterService } from '../services/toaster.service';
import { SecurityService } from '../services/security.service';
import { UtilsService } from '../services/utils.service';
import { TranslateService } from '@ngx-translate/core';

import { Quiz } from '../services/model/quiz';
import { Question } from '../services/model/question';
import { Response } from '../services/model/response';
import { ResponseResult } from '../services/model/response-result';

import ChartDataLabels from 'chartjs-plugin-datalabels';
import { base64StringToBlob } from 'blob-util';

@Component({
  selector: 'quiz-maker',
  templateUrl: './quiz-maker.component.html',
  styleUrls: ['./quiz-maker.component.scss']
})
export class QuizMakerComponent implements OnInit {

  quiz: Quiz;
  questionResponses: any;
  result: any;
  chartOptions: any;
  downloadDisabled: boolean;
  showGoToManager: boolean;

  @ViewChild('pdfTable', {static: false}) pdfTable: ElementRef;
  @ViewChild('chartImg', {static: false}) chartImg: ElementRef;

  downloadingPdf: boolean;

  constructor(
    private quizService: QuizService,
    private toasterService: ToasterService,
    private translateService: TranslateService,
    private route: ActivatedRoute,
    private securityService: SecurityService) {

      this.downloadDisabled = false;
      this.showGoToManager = false;
      this.downloadingPdf = false;
    }

  ngOnInit() {

    this._checkGoToManager();

    this.chartOptions = {
      legend: {
          position: 'bottom',
          pieceLabel: {
             mode: 'value'
          },
          labels: {
            fontColor: 'black'
          }
      },
      tooltips: {
          enabled: false,
          mode: 'single',
          callbacks: {
              label: function(tooltipItems, data) {
                return data.datasets[0].data[tooltipItems.index] + '%';
              }
          }
      },
      plugins: {
        datalabels: {
          color: 'white',
          backgroundColor: '#383838c4',
          borderRadius: 10,
          font: {
            weight: 'bold'
          },
          formatter: function(value, context) {
            return value + '%';
          }
        }
      }
    };
    
    let id: number = Number(this.route.snapshot.paramMap.get('id'));

    this._initQuiz(id);
  }

  private _checkGoToManager() : void {

    this.securityService.checkAdmin().then((check: boolean) => {

      this.showGoToManager = check;

    }).catch(UtilsService.handleError);
  }

  private _initQuiz(
    quizId: number) : void {

    this.quizService.fullGet(quizId).then((quiz: Quiz) => {

      this.questionResponses = {};
      forEach(quiz.questions, (question: Question, indx: number) => {

        this.questionResponses[question.id] = [];

      });

      this.quiz = quiz;

    }).catch(e => this.toasterService.error(e.error));
  }

  showResult() : void {

    let result: any = {};
    let resultIds: number[] = [];

    forEach(this.quiz.questions, (question: Question, indx: number) => {

      let response: Response[] = this.questionResponses[question.id];
      forEach(response, (response: Response, indy: number) => {

        let responseResults: ResponseResult[] = response.responseResults;
        forEach(responseResults, (responseResult: ResponseResult, indz: number) => {
          
          let r: ResponseResult = result[responseResult.resultId];
          if(isNil(r)) {
            result[responseResult.resultId] = responseResult;
            resultIds.push(responseResult.resultId);
          } else {
            r.weight += responseResult.weight;
            result[responseResult.resultId] = r;
          }
        });
      });
    });

    let finalResults: ResponseResult[] = [];
    forEach(resultIds, (resultId: number, indx: number) => {

      finalResults.push(result[resultId]);
    });

    finalResults = finalResults.sort((a, b) => {
      return b.weight - a.weight;
    });

    let total: number = 0;
    forEach(finalResults, (finalResult: ResponseResult, indx: number) => {

      total += finalResult.weight;
    });

    forEach(finalResults, (finalResult: ResponseResult, indx: number) => {

      finalResult.weight = (finalResult.weight / total) * 100;
    });

    
    let labels: string[] = [];
    let values: number[] = [];
    let colors: string[] = [];
    forEach(finalResults, (finalResult: ResponseResult, indx: number) => {
      labels.push(finalResult.result);
      values.push(Number(finalResult.weight.toFixed(2)));
      colors.push(finalResult.color);
    });
    
    this.downloadDisabled = true;      

    setTimeout(() => {
      
      window.scrollTo(0,document.body.scrollHeight);

      setTimeout(() => {
  
        this.result = {
            labels: labels,
            datasets: [{
              data: values,
              borderColor: '#333333',
              backgroundColor: colors,
              hoverBackgroundColor: colors
            }],
            plugin:[ChartDataLabels]   
          };
        }, 500);
      }, 400);
  }

  public downloadAsPDF() {

    let param: any = {};

    let canvas = <HTMLCanvasElement> document.getElementsByTagName("canvas")[0];
    let imageBase64 = canvas.toDataURL('image/png');
    param.image = imageBase64;

    forEach(this.quiz.questions, (question: Question, indx: number) => {

      let responseIds: number[] = this.questionResponses[question.id].map(r => r.id);
      param[question.id] = responseIds;
    });
  
    this.downloadingPdf = true;
    this.quizService.download(
      this.quiz.id, param).then((fileUrl: string) => {

      let filename: string = this.quiz.name.toLocaleUpperCase();
      const link = document.createElement('a');
      link.href = fileUrl;
      link.setAttribute('download', filename);
      document.body.appendChild(link);
      link.click();

      this.toasterService.success(this.translateService.instant("toast.fileDownloadOk"));
  
      this.downloadingPdf = false;

    }).catch(e => {
      this.toasterService.error(e.error);
      this.downloadingPdf = false;
    });
  }

}
