import { Component, OnInit, Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'my-validation-message',
  templateUrl: './my-validation-message.component.html',
  styleUrls: ['./my-validation-message.component.scss']
})
export class MyValidationMessageComponent implements OnInit {

  @Input() private control: AbstractControl;

  constructor(
    private translateService: TranslateService) { }

  ngOnInit() {
  }

  get validationMessage() : string {

    let validationMessage: string = '';
    let dirty: boolean = this.control.dirty;
    let errors: any = this.control.errors;
    if(dirty && errors != undefined && errors != null) {

      if(errors.required) validationMessage += this.translateService.instant('my-validation-message.required');
      if(errors.minlength) validationMessage += this.translateService.instant('my-validation-message.minlength', errors.minlength);
      if(errors.maxlength) validationMessage += this.translateService.instant('my-validation-message.maxlength', errors.maxlength);
    }

    return validationMessage;
  }

}
