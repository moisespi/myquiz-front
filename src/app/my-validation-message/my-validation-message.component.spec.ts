import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyValidationMessageComponent } from './my-validation-message.component';

describe('MyValidationMessageComponent', () => {
  let component: MyValidationMessageComponent;
  let fixture: ComponentFixture<MyValidationMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyValidationMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyValidationMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
