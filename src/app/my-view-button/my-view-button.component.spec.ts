import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyViewButtonComponent } from './my-view-button.component';

describe('MyViewButtonComponent', () => {
  let component: MyViewButtonComponent;
  let fixture: ComponentFixture<MyViewButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyViewButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyViewButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
