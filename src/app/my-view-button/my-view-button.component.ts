import { Component, OnInit, Input } from '@angular/core';
import { clone, cloneDeep } from "lodash";

import { QuizService } from '../services/quiz.service';
import { CommunicationService, CommunicatioType } from '../services/communication.service';
import { ToasterService } from '../services/toaster.service';
import { TranslateService } from '@ngx-translate/core';

import { Quiz } from '../services/model/quiz';

@Component({
  selector: 'my-view-button',
  templateUrl: './my-view-button.component.html',
  styleUrls: ['./my-view-button.component.scss']
})
export class MyViewButtonComponent implements OnInit {


  @Input() quiz: Quiz;

  constructor(
    private quizService: QuizService,
    private communicationService: CommunicationService,
    private toasterService: ToasterService,
    private translateService: TranslateService) { }

  ngOnInit() {
  }

  updateValid() : void {

    let quiz: Quiz = cloneDeep(this.quiz);
    quiz.visible = true;
    this.quizService.updateVisible(quiz).then((quiz: Quiz) => {
    
      this.communicationService.notify(CommunicatioType.FILL_QUIZ_MENU);
      this.toasterService.success(this.translateService.instant("toast.quizSetVisible"));

    }).catch(e => this.toasterService.error(e.error));
  }

  updateInvalid() : void {

    let quiz: Quiz = cloneDeep(this.quiz);
    quiz.visible = false;
    this.quizService.updateVisible(quiz).then((quiz: Quiz) => {
    
      this.communicationService.notify(CommunicatioType.FILL_QUIZ_MENU);
      this.toasterService.success(this.translateService.instant("toast.quizUnsetVisible"));

    }).catch(e => this.toasterService.error(e.error));
  }

}
