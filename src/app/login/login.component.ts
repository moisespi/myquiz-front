import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { UtilsService } from '../services/utils.service';
import { SecurityService } from '../services/security.service';
import { ToasterService } from '../services/toaster.service';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

import { PropertiesEnum } from 'src/app/services/model/properties.enum';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  showPassword: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private utilsService: UtilsService,
    private securityService: SecurityService,
    private toasterService: ToasterService,
    private translateService: TranslateService,
    private router: Router) { }

  ngOnInit() {

    this.showPassword = false;

    this._initForm();
  }

  private _initForm() : void {

    this.loginForm = this.formBuilder.group({
      password: [null, [Validators.required]]
    });
  }

  toggleChangePassword() : void {

    this.showPassword = !this.showPassword;
  }

  login() : void {

    this.utilsService.validateForm(this.loginForm).then((formValue: any) => {

      this.securityService.checkPassword(formValue.password).then((check: boolean) => {

        if(check) {
          localStorage[PropertiesEnum.LOCAL_STORAGE_PASSWORD] = formValue.password;
          this.router.navigateByUrl('/quiz');
        } else {
          this.toasterService.error(this.translateService.instant("toast.loginError"));
        }
  
      }).catch(e => this.toasterService.error(e.error));
    });
  }

}
