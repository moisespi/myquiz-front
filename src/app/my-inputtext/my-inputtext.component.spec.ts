import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyInputtextComponent } from './my-inputtext.component';

describe('MyInputtextComponent', () => {
  let component: MyInputtextComponent;
  let fixture: ComponentFixture<MyInputtextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyInputtextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyInputtextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
