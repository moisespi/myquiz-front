import { Component, OnInit, Input } from '@angular/core';

import { FormGroup, AbstractControl } from "@angular/forms";

@Component({
  selector: 'my-inputtext',
  templateUrl: './my-inputtext.component.html',
  styleUrls: ['./my-inputtext.component.scss']
})
export class MyInputtextComponent implements OnInit {

  @Input() id: string;
  @Input() form: FormGroup;
  @Input() label: string;

  control: AbstractControl;

  constructor() { }

  ngOnInit() {
    
    this.control = this.form.controls[this.id];
  }

}
