import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { isNil } from "lodash";

import { Quiz } from 'src/app/services/model/quiz';
import { QuizService } from 'src/app/services/quiz.service';
import { UtilsService } from '../services/utils.service';
import { ToasterService } from '../services/toaster.service';
import { CommunicationService, CommunicatioType } from '../services/communication.service';
import { ConfirmationService } from 'primeng/api';

import { TranslateService } from '@ngx-translate/core';

import { Subscription } from 'rxjs';

@Component({
  selector: 'quiz-menu',
  templateUrl: './quiz-menu.component.html',
  styleUrls: ['./quiz-menu.component.scss']
})
export class QuizMenuComponent implements OnInit, OnDestroy {

  quizArray: Quiz[];
  selectedQuiz: Quiz;

  addQuizModalVisible: boolean;
  quizForm: FormGroup
  modalHeader: string;
  saveBtnLabel: string;
  
  fillQuizSubscription: Subscription;

  constructor(
    private utilsService: UtilsService,
    private translateService: TranslateService,
    private formBuilder: FormBuilder,
    private toasterService: ToasterService,
    private confirmationService: ConfirmationService,
    private communicationService: CommunicationService,
    private quizService: QuizService) { }

  ngOnInit() {

    this.addQuizModalVisible = false;

    this.initQuizForm();
    this._fillQuizMenu();

    this.fillQuizSubscription = this.communicationService.observable(CommunicatioType.FILL_QUIZ_MENU).subscribe((param: any) => {
      this._fillQuizMenu();
    })
  }

  ngOnDestroy() {

    this.fillQuizSubscription.unsubscribe();
  }

  private _fillQuizMenu() : void {

    this.quizService.list().then((quizArray: Quiz[]) => {

      this.quizArray = quizArray;

    }).catch(UtilsService.handleError);
  }

  private initQuizForm() : void {

    this.quizForm = this.formBuilder.group({
      id: [null, []],
      name: [null, [Validators.required, Validators.maxLength(64)]]
    });
  }

  addQuizClick() : void {

    this.quizForm.reset();
    this.modalHeader = this.translateService.instant("quiz-menu.addQuizDialog.headerAdd");
    this.saveBtnLabel = this.translateService.instant("basic.add");

    this.addQuizModalVisible = true;
  }

  modifyQuizClick(quiz: Quiz) : void {

    this.quizForm.patchValue(quiz);
    this.modalHeader = this.translateService.instant("quiz-menu.addQuizDialog.headerModify");
    this.saveBtnLabel = this.translateService.instant("basic.modify");

    this.addQuizModalVisible = true;
  }
  
  addQuizModalCreate() : void {

    this.utilsService.validateForm(this.quizForm).then((quiz: Quiz) => {
      
      if(isNil(quiz.id)) {

        this.quizService.create(quiz).then((quiz: Quiz) => {
  
          this._fillQuizMenu();
          this.addQuizModalVisible = false;
          this.toasterService.success(this.translateService.instant("toast.quizAdded"));
    
        }).catch(e => this.toasterService.error(e.error));
      } else {
          
        this.quizService.update(quiz).then((quiz: Quiz) => {
    
          this._fillQuizMenu();
          this.addQuizModalVisible = false;
          this.toasterService.success(this.translateService.instant("toast.quizUpdated"));
    
        }).catch(e => this.toasterService.error(e.error));
      }
    });
  }

  removeQuizClick(quiz: Quiz) : void {

    this.confirmationService.confirm({
        message: this.translateService.instant('confirmation.deleteQuizMessage'),
        accept: () => {
          
          this.quizService.delete(quiz).then((quiz: Quiz) => {
      
            this._fillQuizMenu();
            this.clearQuizSelect();
            this.toasterService.success(this.translateService.instant("toast.quizRemoved"));
      
          }).catch(e => this.toasterService.error(e.error));
        }
    });
  }

  addQuizModalCancel() : void {

    this.addQuizModalVisible = false;
  }

  onQuizSelect(quizSelectionEvent: any) : void {

    let quiz: Quiz = quizSelectionEvent.data;

    this.communicationService.notify(CommunicatioType.QUIZ_SELECTED, quiz);
  }

  clearQuizSelect() : void {

    this.communicationService.notify(CommunicatioType.QUIZ_SELECTED, null);
  }

}
