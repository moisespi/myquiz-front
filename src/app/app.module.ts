import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { QuizMenuComponent } from './quiz-menu/quiz-menu.component';

// Services
import { MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';

// Primeng modules
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { ToastModule } from 'primeng/toast';
import { InputTextModule } from 'primeng/inputtext';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { TabViewModule } from 'primeng/tabview';
import { ColorPickerModule } from 'primeng/colorpicker';
import { DropdownModule } from 'primeng/dropdown';
import { SliderModule } from 'primeng/slider';
import { CheckboxModule } from 'primeng/checkbox';
import { ChartModule } from 'primeng/chart';
import { PasswordModule } from 'primeng/password';

import { MyInputtextComponent } from './my-inputtext/my-inputtext.component';
import { MyValidationMessageComponent } from './my-validation-message/my-validation-message.component';

import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
import { QuizDetailsComponent } from './quiz-details/quiz-details.component';
import { QuestionComponent } from './question/question.component';
import { MyInputcolorComponent } from './my-inputcolor/my-inputcolor.component';
import { QuizListComponent } from './quiz-list/quiz-list.component';
import { QuizManagerComponent } from './quiz-manager/quiz-manager.component';
import { QuizMakerComponent } from './quiz-maker/quiz-maker.component';
import { MyViewButtonComponent } from './my-view-button/my-view-button.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    QuizMenuComponent,
    MyInputtextComponent,
    MyValidationMessageComponent,
    QuizDetailsComponent,
    QuestionComponent,
    MyInputcolorComponent,
    QuizListComponent,
    QuizManagerComponent,
    QuizMakerComponent,
    MyViewButtonComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule, BrowserAnimationsModule,
    HttpClientModule, AppRoutingModule,
    FormsModule, ReactiveFormsModule,
    TableModule, ButtonModule, DialogModule, ToastModule, InputTextModule,
    ConfirmDialogModule, TabViewModule, ColorPickerModule, DropdownModule,
    SliderModule, CheckboxModule, ChartModule, PasswordModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  })
  ],
  providers: [
    MessageService,
    ConfirmationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}
